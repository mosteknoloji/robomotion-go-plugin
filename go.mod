module robomotion-go-plugin

go 1.13

require (
	bitbucket.org/mosteknoloji/robomotion-go-lib v0.0.0-20200511101604-ec695d11e28c
	github.com/golang/protobuf v1.3.5 // indirect
	github.com/hashicorp/go-hclog v0.12.1 // indirect
	github.com/hashicorp/go-plugin v1.2.0 // indirect
	github.com/magiconair/properties v1.8.1 // indirect
	github.com/tidwall/gjson v1.6.0 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	google.golang.org/grpc v1.28.0 // indirect
)
