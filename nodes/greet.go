package nodes

import (
	"encoding/json"

	"bitbucket.org/mosteknoloji/robomotion-go-lib/runtime"
)

// Foo is the implementation of a Robomotion node that can be used in Robomotion Designer.
//
// It may take some properties which can be set through designer in addition to common properties
// defined in go-lib library.
//
// Full name of the node has to be tagged to SNode field in order to distinguish nodes on runtime.
// It should satisfy the structure "Namespace.NodeName" where Namespace indicates where the plugin node
// belongs to and NodeName indicates what the plugin node is called.
type Greet struct {
	runtime.SNode `id:"Robomotion.Example.Greeter" name:"Greet"`

	OutText runtime.Variable `title:"Text" type:"string" scope:"Message" name:"text" messageScope:""`
}

// OnCreate is called when a flow is started for each node present in Robomotion Designer,
// and takes a config parameter consisting of node properties set through the designer interface.
//
// Config is unmarshalled into the struct declared above before the function is called.
func (n *Greet) OnCreate(config []byte) error {
	return nil
}

// OnMessage ...
func (n *Greet) OnMessage(inMessage []byte) (outMessage []byte, err error) {

	msg := make(map[string]interface{})
	json.Unmarshal(inMessage, &msg)

	outMessage, _ = runtime.SetStringVariable(&n.OutText, inMessage, "Hello World!")
	return outMessage, nil
}

// OnClose ...
func (n *Greet) OnClose() error {
	return nil
}
