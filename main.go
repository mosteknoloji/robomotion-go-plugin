package main

import (
	"bitbucket.org/mosteknoloji/robomotion-go-lib/runtime"
)

// No changes have to be done below.
func main() {
	runtime.Start()
}
