package main

import (
	"bitbucket.org/mosteknoloji/robomotion-go-lib/runtime"

	"robomotion-go-plugin/nodes"
)

// Any preperations that should be done before the plugin starts to execute
// could be done before or after InitNodes function call.
//
// All nodes implemented inside that plugin should be initialized by passing
// each node's reference to the InitNodes function.
func init() {
	runtime.InitNodes(&nodes.Greet{})
}
